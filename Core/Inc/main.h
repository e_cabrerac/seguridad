/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define ventana_Pin GPIO_PIN_4
#define ventana_GPIO_Port GPIOE
#define ventana_EXTI_IRQn EXTI4_IRQn
#define pir_Pin GPIO_PIN_15
#define pir_GPIO_Port GPIOC
#define pir_EXTI_IRQn EXTI15_10_IRQn
#define fila1_Pin GPIO_PIN_3
#define fila1_GPIO_Port GPIOC
#define on_off_Pin GPIO_PIN_0
#define on_off_GPIO_Port GPIOA
#define on_off_EXTI_IRQn EXTI0_IRQn
#define fila2_Pin GPIO_PIN_1
#define fila2_GPIO_Port GPIOA
#define fila3_Pin GPIO_PIN_2
#define fila3_GPIO_Port GPIOA
#define fila4_Pin GPIO_PIN_3
#define fila4_GPIO_Port GPIOA
#define columna1_Pin GPIO_PIN_4
#define columna1_GPIO_Port GPIOA
#define columna2_Pin GPIO_PIN_5
#define columna2_GPIO_Port GPIOA
#define columna3_Pin GPIO_PIN_6
#define columna3_GPIO_Port GPIOA
#define columna4_Pin GPIO_PIN_7
#define columna4_GPIO_Port GPIOA
#define red_led_Pin GPIO_PIN_14
#define red_led_GPIO_Port GPIOD
#define blue_led_Pin GPIO_PIN_15
#define blue_led_GPIO_Port GPIOD
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
