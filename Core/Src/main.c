/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "MY_Keypad4x4.h"
#include "lcd16x2_i2c.h"
#include <stdbool.h>
#include <stdlib.h>
#include "stm32f4xx_hal.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
volatile bool alarma = false; //true: alarma encendida
volatile bool sistema = false; //true: sistema on, false: sistema off
volatile uint32_t luz;
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

I2C_HandleTypeDef hi2c1;

TIM_HandleTypeDef htim1;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
static void MX_TIM1_Init(void);
static void MX_ADC1_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
bool mySwitches[16]; //almacena el estado de todas las teclas del teclado
uint8_t value = 0;  // valor del duty cycle

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	Keypad_WiresTypeDef myKeypadStruct; //Estructura teclado
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_TIM1_Init();
  MX_ADC1_Init();
  /* USER CODE BEGIN 2 */

  /*INICIALIZAR PWM (BUZZER)*/
  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_2);

  //Inicializar ADC en modo interrupción
  HAL_ADC_Start_IT(&hadc1);


  /*ESTRUCTURA TECLADO*/
      	  //Filas - puertos
      myKeypadStruct.IN0_Port = GPIOC;
      myKeypadStruct.IN1_Port = GPIOA;
      myKeypadStruct.IN2_Port = GPIOA;
      myKeypadStruct.IN3_Port = GPIOA;
      	  //Columnas - puertos
      myKeypadStruct.OUT0_Port = GPIOA;
      myKeypadStruct.OUT1_Port = GPIOA;
      myKeypadStruct.OUT2_Port = GPIOA;
      myKeypadStruct.OUT3_Port = GPIOA;
      	  //Filas - pines
      myKeypadStruct.IN0pin = GPIO_PIN_3;
      myKeypadStruct.IN1pin = GPIO_PIN_1;
      myKeypadStruct.IN2pin = GPIO_PIN_2;
      myKeypadStruct.IN3pin = GPIO_PIN_3;
      	  //Columnas - pines
      myKeypadStruct.OUT0pin = GPIO_PIN_4;
      myKeypadStruct.OUT1pin = GPIO_PIN_5;
      myKeypadStruct.OUT2pin = GPIO_PIN_6;
      myKeypadStruct.OUT3pin = GPIO_PIN_7;

      /*INICIALIZACIÓN TECLADO*/
      Keypad4x4_Init(&myKeypadStruct);

      /*INICIALIZACIÓN PANTALLA LCD*/
      lcd16x2_i2c_init(&hi2c1);

      /*PIN*/
      uint8_t pin_correct[4] = {0, 0, 0, 0}; //Pin correcto
      uint8_t pin_introduced[4]; //Pin introducido por el usuario
      uint8_t j = 0; //indice para el pin


  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  //Asignar valor al buzzer (pwm)
	  htim1.Instance->CCR2 = value; //variar el duty cycle

	  //Encender led azul cuando haya poca luz
	  if(luz < 550)
		  HAL_GPIO_WritePin(blue_led_GPIO_Port, blue_led_Pin, 1);
	  else
		  HAL_GPIO_WritePin(blue_led_GPIO_Port, blue_led_Pin, 0);

	//SISTEMA ON
	if (sistema) {
		//Intruso detectado
		if (alarma){
			HAL_GPIO_WritePin(red_led_GPIO_Port, red_led_Pin, 1); //encender led rojo
			value = 255; //buzzer on
			/*INTRODUCIR PIN*/
			//Imprimir "PIN: " en la pantalla
			lcd16x2_i2c_1stLine(); //Mover cursor al inicio de la primera línea
			lcd16x2_i2c_printf("PIN:");

			//Mostrar pin introducido
			for(uint8_t i = 0; i < j; i++){
				lcd16x2_i2c_setCursor(2, i); //Mover cursor a la siguiente cifra
				lcd16x2_i2c_printf("%d", pin_introduced[i]);
			}
			//Parpadeo del cursor
			lcd16x2_i2c_setCursor(2, j);
			lcd16x2_i2c_cursorShow(true);

			//Todavía no se han introducido 4 cifras
			if(j < 4){
				Keypad4x4_ReadKeypad(mySwitches); //Almacena en mySwitches las teclas pulsadas
				//Bucle que recorre mySwitches
				for(uint8_t i = 0; i < 16; i++){
					//Si hay pulsada una tecla se almacena en pin_introduced
					if(mySwitches[i]){
						pin_introduced[j] = atoi(Keypad4x4_GetChar(i)); //caracter correspondiente a mySwitches[i]
						j++; //incrementa en 1 el nº de cifras introducidas
					}
				}
			}

			//Se han introducido las 4 cifras del pin
			else{
				lcd16x2_i2c_clear(); //Vacía la pantalla
				lcd16x2_i2c_1stLine(); //Mover cursor al inicio de la primera línea
				bool correct = false;
				//Compara el pin introducido y el correcto
				for(uint8_t i = 0; i < 4; i++){
					if(pin_introduced[i] != pin_correct[i])
						break;
					else if(i == 3)
						correct = true;
				}

				//Pin correcto:
				if(correct){
					alarma = false; //apagar alarma
					//mensaje por pantalla
					lcd16x2_i2c_printf("ALARMA");
					lcd16x2_i2c_2ndLine();
					lcd16x2_i2c_printf("DESACTIVADA");
					lcd16x2_i2c_1stLine();
				}
				//Pin incorrecto:
				else
					lcd16x2_i2c_printf("PIN INCORRECTO");
				HAL_Delay(500);
				j = 0;
				correct = false;
			}
		}

		//Intruso no detectado
		else{
			HAL_GPIO_WritePin(red_led_GPIO_Port, red_led_Pin, 0); //apagar led rojo
			value = 0; //buzzer off
			lcd16x2_i2c_printf("No hay intrusos");
		}
	}

	//SISTEMA OFF
	else
		lcd16x2_i2c_printf("SISTEMA OFF");

	HAL_Delay(100);
	lcd16x2_i2c_clear();
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.ScanConvMode = DISABLE;
  hadc1.Init.ContinuousConvMode = ENABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
  */
  sConfig.Channel = ADC_CHANNEL_8;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_480CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 691;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 254;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, columna1_Pin|columna2_Pin|columna3_Pin|columna4_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, red_led_Pin|blue_led_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : ventana_Pin */
  GPIO_InitStruct.Pin = ventana_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(ventana_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : pir_Pin */
  GPIO_InitStruct.Pin = pir_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(pir_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : fila1_Pin */
  GPIO_InitStruct.Pin = fila1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(fila1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : on_off_Pin */
  GPIO_InitStruct.Pin = on_off_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(on_off_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : fila2_Pin fila3_Pin fila4_Pin */
  GPIO_InitStruct.Pin = fila2_Pin|fila3_Pin|fila4_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : columna1_Pin columna2_Pin columna3_Pin columna4_Pin */
  GPIO_InitStruct.Pin = columna1_Pin|columna2_Pin|columna3_Pin|columna4_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : red_led_Pin blue_led_Pin */
  GPIO_InitStruct.Pin = red_led_Pin|blue_led_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

  HAL_NVIC_SetPriority(EXTI4_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI4_IRQn);

  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

}

/* USER CODE BEGIN 4 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){
	//Botón on/off (solo cuando la alarma no está sonando)
	if((GPIO_Pin == on_off_Pin) && (alarma == false)){
		if(sistema)
			sistema = false; //apagar si estaba encendido
		else
			sistema = true; //encender si estaba apagado
	}

	//Ventana abierta con sistema conectado
	else if((GPIO_Pin == GPIO_PIN_4) && (sistema == true) && (alarma == false)){
		if(HAL_GPIO_ReadPin(ventana_GPIO_Port, ventana_Pin) == 1)
			alarma = true; //ventana abierta
	}

	//Detección de presencia (sensor PIR) con sistema conectado
	else if((GPIO_Pin == pir_Pin) && (sistema == true) && (alarma == false))
		alarma = true;
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc){
	//LDR
	if(hadc->Instance == ADC1)
		luz = HAL_ADC_GetValue (&hadc1);
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
